firebaseAuth.$inject = ["$location", "firebaseApp"];
export default function firebaseAuth($location, $firebaseApp) {
  var user = "";
  var auth = $firebaseApp.auth();
  
  return {
    firebaseAuth: auth,
    getUser: function () {
      if (user == "") {
        user = localStorage.getItem("userPassword");
      }
      return user;
    },
    setUser: function (value) {
      localStorage.setItem("userPassword", value);
      user = value;
    },
    logoutUser: function () {
      auth
        .signOut()
        .then(function () {
          user = "";
          console.log(localStorage);
          localStorage.removeItem("userPassword");
          localStorage.clear();
          $location.path("/admin");
          console.log("log out...");
        })
        .catch(function (error) {
          console.log(error);
        });
    },
  };
}
