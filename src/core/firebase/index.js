import angular from 'angular';

import firebaseAuth from "./firebase-auth";
import firebaseApp from "./firebase-app";

const firebaseModule = angular
  .module("core.firebase", ["firebase"])
  .service("firebaseApp", firebaseApp)
  .service("firebaseAuth", firebaseAuth);

  export default firebaseModule;