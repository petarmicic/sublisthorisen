firebaseApp.$inject = [];
export default function firebaseApp() {
  var firebaseConfig = {
    apiKey: "AIzaSyBVo0Dc02hADQvaNAqq9Q6YSIt2hNQute8",
    authDomain: "sublist1-8f171.firebaseapp.com",
    databaseURL: "https://sublist1-8f171.firebaseio.com",
    projectId: "sublist1-8f171",
    storageBucket: "sublist1-8f171.appspot.com",
    messagingSenderId: "80184366633",
    appId: "1:80184366633:web:1a535b316f39578a512e38",
  };

  var firebaseApp = firebase.initializeApp(firebaseConfig);

  return firebaseApp;
}
