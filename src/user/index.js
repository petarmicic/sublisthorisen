import angular from 'angular';
import coreFirebaseModule from '../core/firebase'
import userRoutes from './routes';
import userHomeFormComponent from './components/home-form/home-form.component';
import userThankYouComponent from './pages/thankyou-comp/thankyou-comp.component';
import userPageComponent from './pages/home-page/home-page.component';


export default angular.module("app.user", [
    coreFirebaseModule.name
]).config(userRoutes)
.component('userHomeForm', userHomeFormComponent)
.component('userThankYou', userThankYouComponent)
.component('userPage', userPageComponent);