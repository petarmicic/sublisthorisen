"use strict";

const userRoutes = [
  "$locationProvider",
  "$routeProvider",
  function config($locationProvider, $routeProvider) {
    $locationProvider.hashPrefix("!");
    $routeProvider
      .when("/", {
        template: "<user-page></user-page>",
      })
      .when("/thankyou", {
        template: "<user-thank-you></user-thank-you>",
      });
  },
];

export default userRoutes;
