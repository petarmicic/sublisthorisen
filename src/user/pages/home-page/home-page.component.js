"use strict";

const userPageComponent = {
  template: require("./home-page.component.html"),
  controller: [
    "$firebaseArray",
    "$location",
    "firebaseApp",
    function userPage($firebaseArray, $location, firebaseApp) {
      var vm = this;
      firebaseApp;
      
      console.log("userPage open");

      vm.addSub = function (userForm) {
        if (userForm.$invalid) {
          console.log("forma nije dobra");
          return;
        }
        var subscriptionsData = firebase.database().ref("subscriptions");
        $firebaseArray(subscriptionsData)
          .$add(vm.userCredentials)
          .then(
            function (subscriptionsData) {
              vm.userCredentials.name = "";
              vm.userCredentials.email = "";
              vm.userCredentials.subs = "";
              $location.path("/thankyou");
            },
            function (error) {
              console.log(error);
            }
          );
      };

      var topicsData = firebase.database().ref("topics");
      vm.userSubscriptions = $firebaseArray(topicsData);
    },
  ],
};

export default userPageComponent;
