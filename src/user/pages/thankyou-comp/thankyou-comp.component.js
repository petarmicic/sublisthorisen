"use strict";

const userThankYouComponent = {
  template: require("./thankyou-comp.component.html"),
  controller: [function userThankYou() {}],
};

export default userThankYouComponent;
