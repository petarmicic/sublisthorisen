"use strict";

var jquery = require("jquery");
import angular from 'angular';
var angularRoute = require("angular-route");
var bootstrapcss = require("bootstrap/dist/css/bootstrap.css");
var angularfire = require("angularfire");
var bootstrap = require("bootstrap");
var version = require("./core/version/version.js");
var versionDirective = require("./core/version/version-directive.js");
var interFilter = require("./core/version/interpolate-filter.js");
var normalizecss = require("html5-boilerplate/dist/css/normalize.css");
var maincss = require("html5-boilerplate/dist/css/main.css");
var appcss = require("./app.css");
var index = require("./index.html");

import coreFirebaseModule from './core/firebase'
import adminModule from './admin';
import userModule from './user';

import sharedComponents from './shared';

console.debug(adminModule);

angular
  .module("app", [
    "ngRoute",
    "firebase",
    "app.admin",
    coreFirebaseModule.name,
    adminModule.name,
    userModule.name,
    sharedComponents.name
  ]);
