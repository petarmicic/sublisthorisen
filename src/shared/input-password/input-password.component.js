"use strict";

const inputPasswordComponent = {
  template: require("./input-password.component.html"),
  require: {
    $ngModel: "ngModel",
  },
  bindings: {
    elId: "@",
    elName: "@",
    showErrors: "<",
  },
  transclude: true,
  controller: [
    function inputPassword() {
      var vm = this;

      this.data = "";

      this.$onInit = () => {
        this.$ngModel.$render = () => {
          this.data = this.$ngModel.$viewValue;

          this.$ngModel.$setPristine();
          this.$ngModel.$setUntouched();
        };
      };

      this.onChange = () => {
        this.$ngModel.$setViewValue(this.data);
        this.$ngModel.$setDirty();
      };
    },
  ],
};

export default inputPasswordComponent;
