"use strict";

const inputCheckboxComponent = {
  template: require("./input-checkbox.component.html"),
  require: {
    $ngModel: "ngModel",
  },
  bindings: {
    elId: "@",
    elName: "@",
  },
  transclude: true,
  controller: [
    function inputCheckbox() {
      var vm = this;

      this.$onInit = () => {
        this.$ngModel.$render = () => {
          this.data = this.$ngModel.$viewValue;

          this.$ngModel.$setPristine();
          this.$ngModel.$setUntouched();
        };
      };

      this.onChange = () => {
        this.$ngModel.$setViewValue(this.data);
        this.$ngModel.$setDirty();
      };
    },
  ],
};

export default inputCheckboxComponent;
