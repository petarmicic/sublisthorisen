const passwordStrenghtValidator = function () {
  console.log("passwordStrenghtValidator");
  return {
    restrict: "A",
    require: "ngModel",
    link: function ($s, $e, $a, $ngModel) {
      console.log("passwordStrenghtValidator");
      $ngModel.$validators.passwordLength = (mv, vv) => {
        console.log("passwordLength", vv);
        if (!vv) {
          return false;
        }
        if (vv.length < 8) {
          return false;
        }
        return true;
      };

      $ngModel.$validators.numbersNonExistence = (mv, vv) => {
        return /\d/.test(vv);
      };

      $ngModel.$validators.specialCharactersNonExistence = (mv, vv) => {
        var format = /[ `!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?~]/;
        return format.test(vv);
      };
    },
  };
};

export default passwordStrenghtValidator;
