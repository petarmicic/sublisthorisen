"use strict";

const inputTextComponent = {
  template: require("./input-text.component.html"),
  require: {
    $ngModel: "ngModel",
  },
  bindings: {
    elId: "@",
    elName: "@",
    showErrors: "<",
  },
  transclude: true,
  controller: [
    function inputText() {
      var vm = this;

      this.data = "";

      this.$onInit = () => {
        this.$ngModel.$render = () => {
          this.data = this.$ngModel.$viewValue;

          this.$ngModel.$setPristine();
          this.$ngModel.$setUntouched();
        };
      };

      this.onChange = () => {
        this.$ngModel.$setViewValue(this.data);
        this.$ngModel.$setDirty();
      };
    },
  ],
};

export default inputTextComponent;
