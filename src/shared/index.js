import angular from "angular";
import inputCheckboxComponent from "./input-checkbox/input-checkbox.component";
import inputEmailComponent from "./input-email/input-email.component";
import inputPasswordComponent from "./input-password/input-password.component";
import inputTextComponent from "./input-text/input-text.component";
import passwordStrenghtValidator from "./validators/password-strength.component";

export default angular
  .module("app.shared", [])
  .component("inputEmail", inputEmailComponent)
  .component("inputPassword", inputPasswordComponent)
  .component("inputText", inputTextComponent)
  .component("inputCheckbox" , inputCheckboxComponent)
  .directive("passwordStrenghtValidator", passwordStrenghtValidator);
