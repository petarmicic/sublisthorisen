"use strict";

const adminSubscriptionFormComponent = {
  template: require("./subscription-form.component.html"),
  require: {
    $ngModel: "ngModel",
  },
  bindings: {
    userData: "<",
  },
  controller: [
    function adminSubscriptionForm() {
      var vm = this;

      vm.$onInit = function () {};

      vm.deleteSub = function (user) {
        console.log(user);
        vm.userData.$remove(user).then(
          function (ref) {
            console.log(user);
          },
          function (error) {
            console.log(error);
          }
        );
      };
    },
  ],
};

export default adminSubscriptionFormComponent;
