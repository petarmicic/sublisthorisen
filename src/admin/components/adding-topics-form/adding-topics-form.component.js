"use strict";

let instanceCounter = 0;
const adminTopicsFormComponent = {
  template: require("./adding-topics-form.component.html"),
  require: {
    $ngModel: "ngModel",
  },
  bindings: {
    showErrors: "<",
  },
  controller: [
    function adminTopicsForm() {
      var vm = this;

      vm.data = "";

      vm.$onInit = function () {
        vm.$ngModel.$formatters.push((modelValue) => {
          let output = {
            name: modelValue ? modelValue.name : "",
          };
          return output;
        });
        vm.$ngModel.$parsers.push((viewValue) => {
          let output = {
            name: viewValue ? viewValue.name : "",
          };
          return output;
        });

        vm.$ngModel.$validators.topicName = ($modelValue, $viewValue) => {
          return !!$viewValue && !!$viewValue.name;
        };

        vm.$ngModel.$render = () => {
          vm.data = vm.$ngModel.$viewValue;
          vm.$ngModel.$setPristine();
          vm.$ngModel.$setUntouched();
        };
      };

      instanceCounter++, (vm.instanceCounter = instanceCounter);

      vm.onChange = () => {
        vm.$ngModel.$setViewValue({ ...vm.data });
        vm.$ngModel.$setDirty();
      };
    },
  ],
};

export default adminTopicsFormComponent;
