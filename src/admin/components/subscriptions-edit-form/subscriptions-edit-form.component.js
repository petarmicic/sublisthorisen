"use strict";

let instanceCounter = 0;
const adminSubscriptionsEditFormComponent = {
  template: require("./subscriptions-edit-form.component.html"),
  require: {
    $ngModel: "ngModel",
  },
  bindings: {
    showErrors: "<",
    userSubscriptions: "<",
  },
  controller: [
    function adminSubscriptionsEditForm() {
      var vm = this;

      vm.$onInit = function () {
        vm.$ngModel.$formatters.push((modelValue) => {
          var output = {
            name: modelValue ? modelValue.name : "",
            email: modelValue ? modelValue.email : "",
            subs: modelValue ? modelValue.subs : "",
          };

          return output;
        });

        vm.$ngModel.$parsers.push((viewValue) => {
          let output = {
            name: viewValue ? viewValue.name : "",
            email: viewValue ? viewValue.email : "",
            subs: viewValue ? viewValue.subs : "",
          };

          return output;
        });

        vm.$ngModel.$validators.name = ($modelValue, $viewValue) => {
          return !!$viewValue && !!$viewValue.name;
        };

        vm.$ngModel.$validators.email = ($modelValue, $viewValue) => {
          return !!$viewValue && !!$viewValue.email;
        };

        vm.$ngModel.$validators.subs = ($modelValue, $viewValue) => {
          return !!$viewValue && !!$viewValue.subs;
        };

        vm.$ngModel.$validators.length = ($modelValue, $viewValue) => {
          if (!$viewValue) {
            return false;
          }

          if ($viewValue.length < 4) {
            return false;
          }

          return true;
        };

        vm.$ngModel.$render = () => {
          vm.user = {};
          vm.user.name = vm.$ngModel.$viewValue.name;
          vm.user.email = vm.$ngModel.$viewValue.email;
          vm.user.subs = vm.$ngModel.$viewValue.subs;
          vm.$ngModel.$setPristine();
          vm.$ngModel.$setUntouched();
        };
      };

      instanceCounter++, (vm.instanceCounter = instanceCounter);

      vm.onChange = () => {
        vm.$ngModel.$setViewValue({
          name: vm.user.name,
          email: vm.user.email,
          subs: vm.user.subs,
        });
        vm.$ngModel.$setDirty();
      };
    },
  ],
};

export default adminSubscriptionsEditFormComponent;
