"use strict";

let instanceCounter = 0;
const adminLoginFormComponent = {
  template: require("./admin-login-form.component.html"),
  require: {
    $ngModel: "ngModel",
  },
  bindings: {
    showErrors: "<",
  },
  controller: [
    function adminLoginForm() {
      var vm = this;

      vm.$onInit = function () {
        vm.$ngModel.$formatters.push((modelValue) => {
          let output = {
            email: modelValue ? modelValue.email : "",
            password: modelValue ? modelValue.password : "",
          };

          return output;
        });

        vm.$ngModel.$parsers.push((viewValue) => {
          let output = {
            email: viewValue ? viewValue.email : "",
            password: viewValue ? viewValue.password : "",
          };

          return output;
        });

        vm.$ngModel.$validators.email = ($modelValue, $viewValue) => {
          return !!$viewValue && !!$viewValue.email;
        };

        vm.$ngModel.$validators.password = ($modelValue, $viewValue) => {
          return !!$viewValue && !!$viewValue.password;
        };

        vm.$ngModel.$render = () => {
          vm.user = {
            email: "",
            password: "",
          };
          vm.user.email = vm.$ngModel.$viewValue.email;
          vm.user.password = vm.$ngModel.$viewValue.password;
          vm.$ngModel.$setPristine();
          vm.$ngModel.$setUntouched();
        };
      };

      instanceCounter++, (vm.instanceCounter = instanceCounter);

      vm.onChange = () => {
        vm.$ngModel.$setViewValue({
          email: vm.user.email,
          password: vm.user.password,
        });
        vm.$ngModel.$setDirty();
      };

    },
  ],
};

export default adminLoginFormComponent;
