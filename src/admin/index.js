import angular from "angular";
import coreFirebaseModule from "../core/firebase";
import adminRoutes from "./routes";
import adminTopicsFormComponent from "./components/adding-topics-form/adding-topics-form.component";
import adminSubscriptionsEditFormComponent from "./components/subscriptions-edit-form/subscriptions-edit-form.component";
import adminLoginFormComponent from "./components/admin-login-form/admin-login-form.component";
import adminSubscriptionFormComponent from "./components/subscriptions-form/subscription-form.component";
import adminPageComponent from "./pages/admin-login-page/admin-login-page.component";
import adminSubscriptionsEditPageComponent from "./pages/subscriptions-edit-page/subscriptions-edit-page.component";
import adminSubscriptionsPageComponent from "./pages/subscriptions-page/subscriptions-page.component";
import addingTopicsPageComponent from "./pages/adding-topics-page/adding-topics-page.component";
// import adminPlaygroundPageComponent from "./pages/admin-playground-page/admin-playground-page.component";
import sharedComponents from "../shared";

export default angular
  .module("app.admin", [coreFirebaseModule.name, sharedComponents.name])
  .config(adminRoutes)
  .component("adminTopicsForm", adminTopicsFormComponent)
  .component("adminSubscriptionsEditForm", adminSubscriptionsEditFormComponent)
  .component("adminLoginForm", adminLoginFormComponent)
  .component("adminSubscriptionForm", adminSubscriptionFormComponent)
  .component("adminPage", adminPageComponent)
  .component("adminSubscriptionsPage", adminSubscriptionsPageComponent)
  .component("adminSubscriptionsEditPage", adminSubscriptionsEditPageComponent)
  .component("addingTopicsPage", addingTopicsPageComponent);
  // .component("adminPlaygroundPage", adminPlaygroundPageComponent);
