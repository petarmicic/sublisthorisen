"use strict";

const addingTopicsPageComponent = {
  template: require("./adding-topics-page.component.html"),
  controller: [
    "$firebaseArray",
    "$location",
    function addingTopicsPage($firebaseArray, $location) {
      var vm = this;

      vm.addTopic = function (topicForm) {
        if (topicForm.$invalid) {
          return;
        }
        var topicsData = firebase.database().ref("topics");
        $firebaseArray(topicsData)
          .$add(vm.topicData)
          .then(
            function (topicsData) {
              vm.topicData = "";
              $location.path("/admin/subscriptions");
            },
            function (error) {
              console.log(error);
            }
          );
      };
    },
  ],
};

export default addingTopicsPageComponent;
