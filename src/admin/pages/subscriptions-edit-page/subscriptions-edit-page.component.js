"use strict";

const adminSubscriptionsEditPageComponent = {
  template: require("./subscriptions-edit-page.component.html"),
  controller: [
    "$location",
    "$routeParams",
    "$firebaseArray",
    "$firebaseObject",
    function adminSubscriptionsEditPage(
      $location,
      $routeParams,
      $firebaseArray,
      $firebaseObject
    ) {
      var vm = this;

      var id = $routeParams.id;

      var subscriptionsData = firebase.database().ref("subscriptions/" + id);
      vm.editCredentials = $firebaseObject(subscriptionsData);

      var topicsData = firebase.database().ref("topics");
      vm.userSubscriptions = $firebaseArray(topicsData);

      console.log(vm.editCredentials.$id);
      console.log(vm.userSubscriptions);

      vm.editSub = function (editForm) {
        if (editForm.$invalid) {
          console.log("invalid");
          return;
        }
        subscriptionsData
          .update({
            name: vm.editCredentials.name,
            email: vm.editCredentials.email,
            subs: vm.editCredentials.subs,
          })
          .then(
            function (ref) {
              $location.path("/admin/subscriptions");
              vm.editCredentials.name = "";
              vm.editCredentials.email = "";
              vm.editCredentials.subs = "";
            },
            function (error) {
              console.log(error);
            }
          );
      };
    },
  ],
};

export default adminSubscriptionsEditPageComponent;
