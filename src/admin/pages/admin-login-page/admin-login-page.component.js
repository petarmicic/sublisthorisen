"use strict";

const adminPageComponent = {
  template: require("./admin-login-page.component.html"),
  controller: [
    "$location",
    "firebaseAuth",
    "$scope",
    function adminPage($location, firebaseAuth, $scope) {
      var vm = this;

      var user = firebaseAuth.getUser();
      
      if (user) {
        $location.path("/admin/subscriptions");
      }

      vm.logAdmin = function (loginForm) {
        if (loginForm.$invalid) {
          console.log("invalid loginForm");
          return;
        }
        var username = vm.loginCredentials.email;
        var password = vm.loginCredentials.password;

        firebaseAuth.firebaseAuth
          .signInWithEmailAndPassword(username, password)
          .then(function (user) {
            firebaseAuth.setUser(vm.loginCredentials.password);
            $location.path("/admin/subscriptions");
            vm.errMsg = false;
          })
          .catch(function (error) {
            vm.errMsg = true;
            vm.ErrMessage = error.message;
          });
      };

      var authListener = function () {
        firebaseAuth.firebaseAuth.onAuthStateChanged(function (firebaseUser) {
          if (firebaseUser) {
            console.log("loged in as:", firebaseUser.uid);
          } else {
            console.log("loged out");
          }
        });
      };

      $scope.$on("$destroy", () => {
        authListener();
      });
    },
  ],
};

export default adminPageComponent;
