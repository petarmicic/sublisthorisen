"use strict";

const adminSubscriptionsPageComponent = {
  template: require("./subscriptions-page.component.html"),
  controller: [
    "firebaseAuth",
    "$firebaseArray",
    "$scope",
    "$location",
    function adminSubscriptionsPage(firebaseAuth, $firebaseArray, $scope, $location) {
      var vm = this;

      var user = firebaseAuth.getUser();
      
      if (!user) {
        $location.path("/");
      }

      var subscriptionsData = firebase.database().ref("subscriptions");
      vm.userData = $firebaseArray(subscriptionsData);

      var topicsData = firebase.database().ref("topics");
      vm.userSubscriptions = $firebaseArray(topicsData);

      vm.deleteTopic = function (topic) {
        vm.userSubscriptions.$remove(topic).then(
          function (refe) {
            console.log(topic);
          },
          function (error) {
            console.log(error);
          }
        );
      };

      vm.logout = function () {
        firebaseAuth.logoutUser();
      };

      const onAuthStateChanged = firebaseAuth.firebaseAuth.onAuthStateChanged(
        function (firebaseUser) {
          if (firebaseUser) {
            console.log("loged in as:", firebaseUser.uid);
          } else {
            console.log("loged out");
          }
        }
      );

      $scope.$on("$destroy", () => {
        onAuthStateChanged();
      });
    },
  ],
};

export default adminSubscriptionsPageComponent;
