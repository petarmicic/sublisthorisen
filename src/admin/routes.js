"use strict";

const adminRoutes = [
  "$locationProvider",
  "$routeProvider",
  function config($locationProvider, $routeProvider) {
    $locationProvider.hashPrefix("!");
    $routeProvider
      // .when("/admin", {
      //   template: `<admin-page></admin-page>`,
      //   resolve: {
      //     currentAdminUser: [
      //       "firebaseAuth",
      //       (firebaseAuth) => {
      //         if (!firebaseAuth.getUser()) {
      //           return $location.path("/admin");
      //         }

      //         return firebaseAuth.getUser();
      //       },
      //     ],
      //   },
      //   // $resolve || currentAdminUser
      //   controller: ["currentAdminUser", (currentAdminUser) => {}],
      // })
      .when("/admin", {
        template: "<admin-page></admin-page>",
      })
      .when("/admin/subscriptions", {
        template: `<admin-subscriptions-page ng-model="subs"></admin-subscriptions-page>`,
      })
      .when("/admin/addtopic", {
        template: "<adding-topics-page></adding-topics-page>",
      })
      .when("/admin/edit/:id", {
        template:
          "<admin-subscriptions-edit-page></admin-subscriptions-edit-page>",
      })
      // .when("/admin/playground", {
      //   template: "<admin-playground-page></admin-playground-page>",
      // })
      .otherwise("/");
  },
];

export default adminRoutes;
