var webpack = require('webpack');
var path = require('path');
const TerserPlugin = require('terser-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const HtmlWebpackPlugin = require("html-webpack-plugin");


module.exports = {
  mode: 'development',
  entry: './src/app.js',
  optimization: {
    minimize: true,
    minimizer: [new TerserPlugin()],
  },
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'bundle.js'
  },
  plugins: [

    new webpack.ProvidePlugin({
    
      // angular: 'angular',
      firebase: 'firebase',
    
    }),
    new HtmlWebpackPlugin({
      filename: path.resolve(__dirname, 'dist') + '/index.html',
      template: path.resolve(__dirname, 'src') + "/index.html"
    })
    
  ],
  module: {
    rules: [
      {
        test: /\.html$/i,
        loader: 'html-loader',
      },
      {
        test: /\.css$/,
        use: [
          'style-loader',
          'css-loader'
        ],

      },
      {
        test: /\.(png|svg|jpg|gif)$/,
        use: [
          'file-loader',
        ],
      },
      {
        test: /\.s[ac]ss$/i,
        use: [
          'style-loader',
          'css-loader',
          'sass-loader',
        ],
      },
      {
        test: /\.m?js$/,
        exclude: /(node_modules)/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['@babel/preset-env']
          }
        }
      },
      // {
      //   test: /npm\.js$/,
      //   loader: 'string-replace-loader',
      //   include: path.resolve('node_modules/firebase/server-auth-node'),
      //   options: {
      //     search: 'require(\'firebase/app\');',
      //     replace: 'require(\'firebase/app\').default;',
      //   },
      // }

    ]
  }
};